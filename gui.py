#!/usr/bin/env python3

# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""



import tkinter as tk
import os, re, glob, sys

import numpy as np

import serial


#%%

mainWindow = tk.Tk()
mainWindow.title("FPGA Serial send")

menu = tk.Frame(mainWindow)
menu.grid(sticky = tk.N)


# Connetti
serialRow = 1

indirizzoLabel = tk.Label(menu, text = "Indirizzo porta seriale")
indirizzoLabel.grid(row = serialRow, column = 0)

indirizzo = tk.StringVar() 
indirizzo.set("COM4") # Default

txtBin = tk.Entry(menu, textvariable = indirizzo)
txtBin.grid(row = serialRow, column = 1)


# Led per controllare stato porta
myCanvas = tk.Canvas(menu, width = 40, height = 40)  # Create 200x200 Canvas widget
myCanvas.grid(row = serialRow, column = 2)
myOval = myCanvas.create_oval(2, 2, 38, 38, )
myCanvas.itemconfig(myOval, fill = "red")



ser = serial.Serial()

def coloraSpia():
    global ser
    
    if ser.is_open: 
        myCanvas.itemconfig(myOval, fill = "green")
    else:
        myCanvas.itemconfig(myOval, fill = "red")


def connetti():
    global ser
    
    tmpAddr = indirizzo.get()
    print(f"Vorrei connettermi a {tmpAddr}")
    
    ser = serial.Serial(tmpAddr)  # open serial port
    ser.baudrate = 921600 # Set baudrate
    ser.baudrate = 2000000 # Set baudrate
    ser.timeout = 1 # Set Timeout
    
    print(f"Stato porta: {ser.is_open}")
    coloraSpia()

    
def disconnetti():
    global ser

    if ser.is_open: 
      print("La porta era aperta, provo a chiuderla")
      ser.close()
      
    print(f"Stato porta: {ser.is_open}")
    coloraSpia()

connectButton = tk.Button(menu, text = "CONNECT", width = 30, 
                          command = connetti)
connectButton.grid(row = serialRow+1, columnspan = 3)

disconnectButton = tk.Button(menu, text = "DISCONNECT", width = 30, 
                             command = disconnetti)
disconnectButton.grid(row = serialRow+2, columnspan = 3)







# Impossta valore

CheckVarBin = tk.IntVar()
CheckVarHex = tk.IntVar()




# Se seleziono un checkbox, l'altro si de seleziona
def cambiaRadioBin():
    print(f"Il CheckVarBin vale {CheckVarBin.get()}")
    if CheckVarBin.get() == 1:
        CheckVarHex.set(0)
    coloraSpia()
    
def cambiaRadioHex():
    print(f"Il CheckVarHex vale {CheckVarHex.get()}")
    if CheckVarHex.get() == 1:
        CheckVarBin.set(0)
    coloraSpia()
    
    




# Corpo centrale
binRow = serialRow + 3
hexRow = binRow + 1


# Label
tk.Label(menu, text = "BIN").grid(row = binRow, column = 0)
tk.Label(menu, text = "HEX").grid(row = hexRow, column = 0)


# Checkbox
checkBin = tk.Checkbutton(menu, text = "Bin", variable = CheckVarBin,
               onvalue = 1, offvalue = 0, command = cambiaRadioBin)
checkBin.grid(row = binRow, column = 1)

checkHex = tk.Checkbutton(menu, text = "Hex", variable = CheckVarHex,
               onvalue = 1, offvalue = 0, command = cambiaRadioHex)
checkHex.grid(row = hexRow, column = 1)

# Default values
CheckVarBin.set(0)
CheckVarHex.set(1)

# Testo da inviare
txtBin = tk.Entry(menu)
txtBin.grid(row = binRow, column = 2)

txtHex = tk.Entry(menu)
txtHex.grid(row = hexRow, column = 2)




# Bottone Write
def scrivi():
    #connetti()
    print("\n\n\n\n\n")
    print(f"Stato porta: {ser.is_open}")

    #print(f"Sto per scrivere qualcosa a {indirizzo.get()}")
    
    if CheckVarHex.get() == 1:
        tmpMex = txtHex.get()
        print(f"Sto per scrivere {tmpMex} su {indirizzo.get()}")
        ser.write(bytearray.fromhex( tmpMex ))
    elif CheckVarBin.get() == 1:
        tmpMex = int(txtBin.get(), 2)
        tmpMex = hex(tmpMex).replace("0x", "")
        print(type(tmpMex))
        print(f"Sto per scrivere {tmpMex} su {indirizzo.get()}")
        ser.write(bytearray.fromhex( tmpMex ))
        
    coloraSpia()
        
    #disconnetti()

writeButton = tk.Button(menu, text = "WRITE", width = 30, command = scrivi)
writeButton.grid(row = hexRow+1, columnspan = 3)



# Tengo la finestra attiva
mainWindow.mainloop()








